package com.example.weekendhomeworkreps.view.second

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.weekendhomeworkreps.adapter.SecondAdapter
import com.example.weekendhomeworkreps.databinding.FragmentSecondBinding
import com.example.weekendhomeworkreps.viewmodel.FloatViewModel
import com.example.weekendhomeworkreps.viewmodel.IntViewModel
import com.example.weekendhomeworkreps.viewmodel.StringViewModel

class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<SecondFragmentArgs>()
    private val stringViewModel by viewModels<StringViewModel>()
    private val intViewModel by viewModels<IntViewModel>()
    private val floatViewModel by viewModels<FloatViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSecondBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when(args.type) {
            "String" -> stringViewModel.getStrings()
            "Int" -> intViewModel.getInts()
            "Float" -> floatViewModel.getFloats()
            else -> stringViewModel.getStrings()
        }

        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }
        binding.rvType.layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)

        stringViewModel.state.observe(viewLifecycleOwner) { strings ->
            setAdapter(strings)
        }
        intViewModel.state.observe(viewLifecycleOwner) { ints ->
            val intStrs = ints.map { it.toString() }
            setAdapter(intStrs)
        }
        floatViewModel.state.observe(viewLifecycleOwner) { floats ->
            val floatStrs = floats.map { it.toString() }
            setAdapter(floatStrs)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setAdapter(list: List<String>) {
        binding.rvType.adapter = SecondAdapter().apply { getItems(list) }
    }
}