package com.example.weekendhomeworkreps.view.first

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weekendhomeworkreps.adapter.FirstAdapter
import com.example.weekendhomeworkreps.databinding.FragmentFirstBinding
import com.example.weekendhomeworkreps.viewmodel.FirstViewModel

class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
    private val firstViewModel by viewModels<FirstViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFirstBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firstViewModel.getTypes()
        binding.rvTypes.layoutManager = LinearLayoutManager(context)
        firstViewModel.state.observe(viewLifecycleOwner) { types ->
            binding.rvTypes.adapter = FirstAdapter().apply { addTypes(types) }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}