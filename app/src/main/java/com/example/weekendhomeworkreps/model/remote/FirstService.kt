package com.example.weekendhomeworkreps.model.remote

interface FirstService {
    suspend fun getTypes(): List<String>
}