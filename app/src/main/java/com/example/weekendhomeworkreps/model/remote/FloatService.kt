package com.example.weekendhomeworkreps.model.remote

interface FloatService {
    suspend fun getFloats() : List<Float>
}