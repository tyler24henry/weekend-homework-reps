package com.example.weekendhomeworkreps.model

import com.example.weekendhomeworkreps.model.remote.FloatService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object FloatRepo {
    private val floatService = object : FloatService {
        override suspend fun getFloats(): List<Float> {
            return listOf(1.1.toFloat(), 1.2.toFloat(), 1.3.toFloat(), 1.4.toFloat(), 1.5.toFloat(), 1.6.toFloat(), 1.7.toFloat(), 1.8.toFloat(), 1.9.toFloat(), 2.0.toFloat())
        }
    }

    suspend fun getFloats() : List<Float> = withContext(Dispatchers.IO) {
        floatService.getFloats()
    }
}