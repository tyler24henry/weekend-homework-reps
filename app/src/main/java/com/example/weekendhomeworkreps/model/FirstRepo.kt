package com.example.weekendhomeworkreps.model

import com.example.weekendhomeworkreps.model.remote.FirstService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object FirstRepo {
    private val firstService = object : FirstService {
        override suspend fun getTypes(): List<String> {
            return listOf("String", "Int", "Float")
        }
    }

    suspend fun getTypes() : List<String> = withContext(Dispatchers.IO) {
        firstService.getTypes()
    }
}