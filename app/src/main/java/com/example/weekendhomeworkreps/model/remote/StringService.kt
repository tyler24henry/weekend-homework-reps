package com.example.weekendhomeworkreps.model.remote

interface StringService {
    suspend fun getStrings() : List<String>
}