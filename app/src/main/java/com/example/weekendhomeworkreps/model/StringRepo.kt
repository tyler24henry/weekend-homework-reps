package com.example.weekendhomeworkreps.model

import com.example.weekendhomeworkreps.model.remote.StringService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object StringRepo {
    private val stringService = object : StringService {
        override suspend fun getStrings(): List<String> {
            return listOf("Hello", "Do", "You", "Know", "The", "Muffin", "Man", "He", "Lives", "On", "Drury", "Lane")
        }
    }

    suspend fun getStrings() : List<String> = withContext(Dispatchers.IO) {
        stringService.getStrings()
    }
}