package com.example.weekendhomeworkreps.model.remote

interface IntService {
    suspend fun getInts() : List<Int>
}