package com.example.weekendhomeworkreps.model

import com.example.weekendhomeworkreps.model.remote.IntService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object IntRepo {
    private val intService = object : IntService {
        override suspend fun getInts(): List<Int> {
            return listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        }
    }

    suspend fun getInts() : List<Int> = withContext(Dispatchers.IO) {
        intService.getInts()
    }
}