package com.example.weekendhomeworkreps.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weekendhomeworkreps.model.IntRepo
import kotlinx.coroutines.launch

class IntViewModel : ViewModel() {
    private val intRepo by lazy { IntRepo }

    private val _state = MutableLiveData<List<Int>>()
    val state: LiveData<List<Int>> = _state

    fun getInts() {
        viewModelScope.launch {
            _state.value = intRepo.getInts()
        }
    }
}