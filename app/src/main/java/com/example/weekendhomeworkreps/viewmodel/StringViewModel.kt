package com.example.weekendhomeworkreps.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weekendhomeworkreps.model.StringRepo
import kotlinx.coroutines.launch

class StringViewModel : ViewModel() {
    private val stringRepo by lazy { StringRepo }

    private val _state = MutableLiveData<List<String>>()
    val state: LiveData<List<String>> = _state

    fun getStrings() {
        viewModelScope.launch {
            _state.value = stringRepo.getStrings()
        }
    }
}