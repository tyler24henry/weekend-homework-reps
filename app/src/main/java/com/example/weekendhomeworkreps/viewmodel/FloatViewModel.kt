package com.example.weekendhomeworkreps.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weekendhomeworkreps.model.FloatRepo
import kotlinx.coroutines.launch

class FloatViewModel : ViewModel() {
    private val floatRepo by lazy { FloatRepo }

    private val _state = MutableLiveData<List<Float>>()
    val state: LiveData<List<Float>> = _state

    fun getFloats() {
        viewModelScope.launch {
            _state.value = floatRepo.getFloats()
        }
    }
}