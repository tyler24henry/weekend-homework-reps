package com.example.weekendhomeworkreps.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weekendhomeworkreps.model.FirstRepo
import kotlinx.coroutines.launch

class FirstViewModel : ViewModel() {
    private val firstRepo by lazy { FirstRepo }

    private val _state = MutableLiveData<List<String>>()
    val state: LiveData<List<String>> = _state

    fun getTypes() {
        viewModelScope.launch {
            _state.value = firstRepo.getTypes()
        }
    }
}