package com.example.weekendhomeworkreps.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weekendhomeworkreps.databinding.ItemTypeBinding

class SecondAdapter : RecyclerView.Adapter<SecondAdapter.SecondViewHolder>() {

    private var items = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SecondViewHolder {
        val binding = ItemTypeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SecondViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SecondViewHolder, position: Int) {
        val item = items[position]
        holder.loadItem(item)
    }

    override fun getItemCount(): Int = items.size

    fun getItems(items: List<String>) {
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    class SecondViewHolder(private val binding: ItemTypeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadItem(item: String) {
            binding.btnType.text = item
        }
    }
}