package com.example.weekendhomeworkreps.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.weekendhomeworkreps.databinding.ItemFirstBinding
import com.example.weekendhomeworkreps.view.first.FirstFragmentDirections

class FirstAdapter : RecyclerView.Adapter<FirstAdapter.FirstViewHolder>() {

    private var types = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FirstViewHolder {
        val binding = ItemFirstBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FirstViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FirstViewHolder, position: Int) {
        val type = types[position]
        holder.loadType(type)
        holder.itemView.setOnClickListener { view ->
            val directions = FirstFragmentDirections.actionFirstFragmentToSecondFragment(type)
            view.findNavController().navigate(directions)
        }
    }

    override fun getItemCount(): Int = types.size

    fun addTypes(types: List<String>) {
        this.types = types.toMutableList()
        notifyDataSetChanged()
    }

    class FirstViewHolder(private val binding: ItemFirstBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadType(type: String) {
            binding.btnString.text = type
        }
    }
}





























